# Remote Linux gaming setup

After I went to college, I found out that I would not have physical access to my desktop PC all the time.
What if I wanted to play a game or use a 3D application that was a bit too demanding for my Ryzen 5625U laptop?
I had to figure out an alternative way of handling this.

I spent quite a long time figuring out what would be the best way to handle this.
For my specific use-case, I found out that [Sunshine] with [Moonlight]
would suit my needs the best. Sunshine is an open-source alternative to
NVIDIA GameStream (which is [dead][gamestream-dead]), that supports all GPUs - not only NVIDIA's.
[Moonlight] is an NVIDIA GameStream client (that also makes it a [Sunshine] client).
Together, they make a very good candidate for high-definition, low-latency streaming.

## Preparing

On Arch Linux, I had to install a bunch of packages to make all 3D applications work correctly.
If you're also using Arch Linux, I would recommend using [Chaotic-AUR] repo, so you don't have to
build a bunch of packages from source. Most of these packages should be available in other
distributions' repositories. Particularly important (if you're using an NVIDIA GPU) are the `cuda`
and `nvidia-utils-nvlax` packages. `nvidia-utils-nvlax` should be usable on other distros
(may go by the name of `nvidia-patch`). If you're not using NVIDIA GPU, feel free to skip these
packages (and also all other that mention `nvidia`).

```
cuda
lib32-libglvnd
lib32-libva
lib32-libva-vdpau-driver
lib32-mesa
lib32-mesa-driver
lib32-nvidia-utils
lib32-vulkan-icd-loader
libglvnd
libva
libva-mesa-driver
libva-nvidia-driver-git
libva-utils
mesa
mesa-utils
mesa-vdpau
nvidia
nvidia-utils-nvlax
opencl-nvidia
vulkan-headers
vulkan-icd-loader
vulkan-tools
```

Verify the drivers are working correctly with:
- `nvidia-smi` (or another vendor alternative)
- `vulkaninfo`
- `vainfo`

With that out of the way, we can proceed with setting up the rest.

## Headless Xorg

In case you don't have Xorg installed, install the package `xorg-server` and `xorg-xinit`.

I wanted my setup to be completely headless, so I had to make a special xorg configuration.
This is **optional**, you only need it if you want a true headless setup like me.
I put this into my `/etc/X11/xorg.conf`
```
Section "Screen"
    Identifier     "Screen0"
    Device         "Device0"
    Monitor        "Monitor0"
    DefaultDepth    24
    Option         "Stereo" "0"
    Option         "AllowEmptyInitialConfiguration" "True"
    Option         "UseDisplayDevice" "DP-0"
    Option         "CustomEDID" "DP-0:/etc/X11/headless/edid-monitor.bin"
    Option         "ConnectedMonitor" "DP-0"
    SubSection     "Display"
        Depth       24
    EndSubSection
EndSection
```
Where you need to use a port that is actually present on your graphics card
(otherwise it will software-render) and also provide an EDID dumped from
a monitor and put it into (in my case) `/etc/X11/headless/edid-monitor.bin`.
[Here's my EDID for AOC CQ32G1](./aoc-cq32g1.edid.bin).

You can also dump your own EDID by grabbing the contents of `/sys/class/drm/<card>-<port>/edid`.

## Sunshine

Install the `sunshine-bin` package. If you use NVIDIA GPU, you **NEED** to have [Sunshine] compiled
with CUDA support (aka while having `cuda` package installed), otherwise you'll have issues
screencasting with NvFBC (you'll instead use the bad, slow X11 screencasting).

Now you can either enable the `sunshine` service (`systemctl --user enable --now sunshine.service`)
and have a persistently running headless Xorg, or what I do, launch Xorg with the application of my
choosing at will via SSH.

On my machine, I use something akin to this:
```
bash -c "(xinit <(echo 'dwm & unclutter & <some-program>') && killall sunshine) & DISPLAY=:0 sunshine"
```
Where `<some-program>` is the program you wish to launch.

Note that this snippet requires the packages `dwm` (window manager)
and `unclutter` (hiding the mouse cursor on inactivity).
What this does is basically creating an on-demand xinit script that launches with Xorg.
The `killall sunshine` part is there to prevent having any zombies if something bad happens.
Afterwards, it just starts `sunshine`.

If, after running the script above, you see something like this:
```
[2023:08:13:13:12:52]: Info: Screencasting with NvFBC
```
You're all set (at least on NVIDIA)!

On AMD, you should probably be screencasting with KMS (I suppose?).

If, however, you see something like this:
```
[2023:08:13:13:17:14]: Info: Screencasting with X11
```
It means it doesn't work correctly. It will work, but will be slow and you might experience
stuttering and tearing.

## Moonlight

This part is quite self-explanatory. You need to install `moonlight-qt`, then add your machine
(if it doesn't find it by itself). There should be some default profiles (Desktop, Steam Big Picture).
You can edit these profiles via [Sunshine WebUI][sunshine-webui], which runs on port `47990`.
I use only the Desktop profile, as I do my app launching manually.

## Conclusion

That's about it. It's not too complicated, but finding all the pieces does take time.

I should also mention that this method works on Wayland as well, but at least for me, with an NVIDIA
graphics card, I find that I get better latency with NvFBC (which only works on Xorg), than wlroots
capture, which is the default on Wayland. You might get good performance with KMS capture, but I wasn't
able to make it work.

I'll include some less important details here, as I use this stuff a lot, but it is not critical.

### Remote access

You may wonder, just how do I access my machine outside my home network? You can use any VPN of your choosing,
my recommendations lie with [Wireguard] (I personally use [Tailscale], which is a layer above [Wireguard]
with NAT punching and many more features) or [ZeroTier], due to the traffic being tunneled over UDP, which has
much better performance than TCP (ex. OpenVPN).

### Running Windows games

If you love playing singleplayer games (or multiplayer games without [intrusive anti-cheat software][anticheat]),
Linux is, for the most part, already ready for you. Valve's Proton (more accurately [Glorious Eggroll's proton-ge-custom
fork][proton-ge-custom]) can run pretty much any game I throw at it, be it a Steam game, or a non-Steam one.

On Arch Linux, you can install [proton-ge-custom] from the Chaotic-AUR (alternatively build it yourself).

It's also highly recommended to install `gstreamer` with all of the plugins
(`gst-plugins-*`, also `lib32-gst-plugins-*`), otherwise you might
experience issues in some games relying on specific codecs and stuff
(one example of such game is NieR Replicant).

Here's a script I use for non-Steam games to run them with proton:
```bash
#!/bin/echo MEANT_TO_BE_SOURCED!

# default runtime values
export PROTON="${PROTON:-/usr/share/steam/compatibilitytools.d/proton-ge-custom/proton}"

export WINE_FULLSCREEN_FSR="${WINE_FULLSCREEN_FSR:-1}"
export WINE_FULLSCREEN_FSR_STRENGTH="${WINE_FULLSCREEN_FSR_STRENGTH:-2}"

export MANGOHUD="${MANGOHUD:-1}"
_MANGOHUD_CONFIG=(
	gpu_stats
	gpu_temp
	gpu_core_clock
	gpu_mem_clock
	gpu_power
	cpu_stats
	cpu_temp
	cpu_power
	cpu_mhz
	vram
	ram
	swap
	no_display
	toggle_hud=Shift_R+F12
)
export MANGOHUD_CONFIG="${MANGOHUD_CONFIG:-$(
	IFS=,
	echo "${_MANGOHUD_CONFIG[*]}"
)}"
# ---

cd "$(dirname "$0")" || (echo "cd failed" >&2 && exit)

STEAM_COMPAT_CLIENT_INSTALL_PATH=/dev/null
STEAM_COMPAT_DATA_PATH="$(realpath ./.prefix)"
mkdir -p "$STEAM_COMPAT_DATA_PATH"

export STEAM_COMPAT_CLIENT_INSTALL_PATH
export STEAM_COMPAT_DATA_PATH

# symlink home directory from outside the prefix
# this allows for easy prefix regeneration without losing data
HOME_DIR="$(realpath ./.home)"
mkdir -p "$HOME_DIR"
USERS_DIR="$STEAM_COMPAT_DATA_PATH/pfx/drive_c/users"
mkdir -p "$USERS_DIR"
STEAMUSER_DIR="$USERS_DIR/steamuser"
[ -d "$STEAMUSER_DIR" ] && [ ! -L "$STEAMUSER_DIR" ] && rm -rf "$STEAMUSER_DIR"
ln -fnrs "$HOME_DIR" "$STEAMUSER_DIR"

EXE_DIR="$(dirname "$EXE_PATH")"
EXE="$(basename "$EXE_PATH")"

case "$1" in
"")
	cd "$EXE_DIR" || (echo "cd failed" >&2 && exit)
	"$PROTON" run "$EXE" "$LAUNCH_OPTIONS"
	;;
*)
	echo "running: $PROTON run" "${@:1}"
	"$PROTON" run "${@:1}"
	;;
esac

```
Thanks to this script, I can easily transfer whole game folder while keeping game settings and save date intact,
safely in the `.home` directory inside the game directory.

This is a generic sourceable script. Thanks to this you don't have to have a massive shell script inside
every non-steam game directory.

Now you can put the following script into the game directory, where you wish to have your prefix and home directory
(usually the game root folder):
```bash
#!/bin/bash

export EXE_PATH="./path/to/game.exe"
export LAUNCH_OPTIONS="-dx11orsomething"

export PROTON="/usr/share/steam/compatibilitytools.d/proton-ge-custom/proton"

# look at that, with proton-ge-custom you can
# have AMD FSR upscaling in any game you choose!
export WINE_FULLSCREEN_FSR=1
export WINE_FULLSCREEN_FSR_STRENGTH=2

# mangohud (requires package `mangohud`), similar to MSI Afterburner + RTSS on Windows
export MANGOHUD=1
export MANGOHUD_CONFIG="no_display,cpu_stats,gpu_stats,vram,ram,toggle_hud=Shift_R+F12"

# source the previous script
. "$HOME"/some/folder/proton-nonsteam.sh
```

### Input

Sunshine can only emulate x360 (on Linux and Windows) and ds4 (on Windows only). While this is enough for most games,
some require special features (motion, adaptive triggers, haptic feedback) and this is not possible to do with just
[Moonlight]. For those cases, I go to USB/IP.

#### USB/IP

is a way of sharing a USB device over the network. You can easily bind a special kernel module driver to the USB device
and then attach to it from another machine.

You'll need to install `usbip` package on both machines.

Type `usbip list -l`. You should get something like this:
```
> usbip list -l
 - busid 1-3 (27c6:639c)
   Shenzhen Goodix Technology Co.,Ltd. : unknown product (27c6:639c)

 - busid 1-4 (054c:0ce6)
   Sony Corp. : DualSense wireless controller (PS5) (054c:0ce6)

 - busid 3-1 (0bda:c829)
   Realtek Semiconductor Corp. : unknown product (0bda:c829)

 - busid 3-4 (0c45:6730)
   Microdia : unknown product (0c45:6730)
```

Here you want to note the `busid` of your soon-to-be shared device. In my case (DualSense), it is `1-4`.

Now let's go bind it.
```
> sudo usbip bind -b 1-4
usbip: error: unable to bind device on 1-4
```
What now? Looks like we haven't loaded the `usbip_host` kernel module. Let's fix that.
```
> sudo modprobe usbip_host
> sudo usbip bind -b 1-4
usbip: info: bind device on busid 1-4: complete
```
Now your device should be ready to share.
```
> usbip list -r localhost
usbip: error: could not connect to localhost:3240: System error
```
Right, we should first start `usbipd`.
```
> sudo systemctl start usbipd.service
> usbip list -r localhost
Exportable USB devices
======================
 - localhost
        1-4: Sony Corp. : DualSense wireless controller (PS5) (054c:0ce6)
           : /sys/devices/pci0000:00/0000:00:08.1/0000:03:00.3/usb1/1-4
           : (Defined at Interface level) (00/00/00)
```
Perfect! Now we can attach it on our second machine.
```
> usbip attach -r <address> -b 1-4
libusbip: error: udev_device_new_from_subsystem_sysname failed
usbip: error: open vhci_driver
```
Yeah, on the client machine (the one receiving the USB over network), you need to have
`vhci_hcd` kernel module loaded.
```
> sudo modprobe vhci_hcd
> sudo usbip attach -r <address> -b 1-4
```
Great! We should now see the device in the output of `usbip port`.
Let's also check if `lsusb` gives us the network-connected USB device.
```
> sudo usbip port
Imported USB devices
====================
Port 00: <Port in Use> at High Speed(480Mbps)
       Sony Corp. : DualSense wireless controller (PS5) (054c:0ce6)
      11-1 -> usbip://<address>:3240/1-4
           -> remote bus/dev 001/009
> lsusb
...
Bus 012 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 011 Device 002: ID 054c:0ce6 Sony Corp. DualSense wireless controller (PS5)
Bus 011 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 009 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
...
```
And indeed. It is there! Now we have a controller attached to one machine, sent over the
network, and acting as if it was really plugged into another machine! Isn't that just cool?

When we're done, we just detach the controller from the client.
```
> sudo usbip detach -p 0
usbip: info: Port 0 is now detached!
```
Here the `0` represents the port number when running `usbip port`.

After that, we can unbind the controller on the host machine.
```
> sudo usbip unbind -b 1-4
usbip: info: unbind device on busid 1-4: complete
```
Now we can unplug the controller!

#### Controller touchpad settings

You could, however, run into another issue with the DualSense (or any other) touchpad.
By default, Linux (or more specifically, `libinput`) automatically grabs the touchpad,
and turns it into a mouse. That's the reason that, after plugging the controller in,
it acts as a laptop touchpad. For the most part, that behavior is great! But
there are some games that actually utilize the touchpad for in-game mechanics.

There are multiple ways to disable this behavior, but I'm gonna stick with only one,
because it's (at least to me it seems) easier to toggle on/off whenever I want.

We'll need the `xorg-xinput` package.

We can run `xinput` to list all input devices.
```
> xinput
⎡ Virtual core pointer                          id=2    [master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer                id=4    [slave  pointer  (2)]
⎜   ↳ QEMU QEMU USB Tablet                      id=7    [slave  pointer  (2)]
⎜   ↳ ImExPS/2 Generic Explorer Mouse           id=9    [slave  pointer  (2)]
⎜   ↳ Logitech Wireless Mouse PID:4038          id=12   [slave  pointer  (2)]
⎜   ↳ Sony Interactive Entertainment DualSense Wireless Controller Touchpad     id=13   [slave  pointer  (2)]
⎣ Virtual core keyboard                         id=3    [master keyboard (2)]
    ↳ Virtual core XTEST keyboard               id=5    [slave  keyboard (3)]
    ↳ Power Button                              id=6    [slave  keyboard (3)]
    ↳ AT Translated Set 2 keyboard              id=8    [slave  keyboard (3)]
    ↳ Keyboard passthrough                      id=10   [slave  keyboard (3)]
    ↳ Touchscreen passthrough                   id=11   [slave  keyboard (3)]
```

And now that we know the touchpad id (`13`), we can disable it.
```
> xinput set-prop 13 "Device Enabled" 0
```

And we're done!

---

### Minor details

If you're running full headless like me, you'll have to add `DISPLAY=:0` before many of the commands.
If you get an error looking something like `cannot open display` or something like that, just add `DISPLAY=:0`
before the command (ex. `xinput` -> `DISPLAY=:0 xinput`).

### Contributing

If you think you can contribute something useful here, please do. I would mainly appreciate AMD/Intel specific
instructions, as I only have an NVIDIA setup at hand, but anything will do!

[sunshine]: https://github.com/LizardByte/Sunshine
[moonlight]: https://github.com/moonlight-stream/moonlight-qt
[gamestream-dead]: https://nvidia.custhelp.com/app/answers/detail/a_id/5436/~/gamestream-end-of-service-notification
[chaotic-aur]: https://aur.chaotic.cx
[sunshine-webui]: https://docs.lizardbyte.dev/projects/sunshine/en/latest/about/usage.html#usage
[anticheat]: https://news.ycombinator.com/item?id=28859962
[proton-ge-custom]: https://github.com/GloriousEggroll/proton-ge-custom
[tailscale]: https://tailscale.com/
[zerotier]: https://www.zerotier.com/
[wireguard]: https://www.wireguard.com/
